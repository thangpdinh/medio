<?php

if ( ! class_exists( 'Themeton_Listpost_Category_Widget' ) ) {
	class Themeton_Listpost_Category_Widget extends WP_Widget {

		function __construct() {
			$widget_ops = array(
				'classname'   => 'latest_blogs',
				'description' => esc_html__( 'List Post Category.', 'themetonaddon' )
			);
			parent::__construct( false, esc_html__( ': List Post Category', 'themetonaddon' ), $widget_ops );
		}

		function widget( $args, $instance ) {
			global $post;
			extract( $args );
			extract( array_merge( array(
				'title'          => '',
				'number_cats'    => 5,
				'exclude_cats'   => '',
				'post_type_cats' => 'post',
				'order_cat'      => '',
				'style'          => '',
			), $instance ) );

			print( $before_widget );

			if ( ! empty( $title ) ) {
				echo "" . $args['before_title'] . $title . $args['after_title'];
			}
			$res_arr = [];
			if ( ! empty( $order_cat ) ) {
				$res_arr = explode( ',', $order_cat );
			} else {
				global $wpdb;
				$sql      = "select distinct term_taxonomy_id from wp_term_relationships wt, wp_posts wp where post_type = '" . $post_type_cats . "' and wt.object_id=wp.ID";
				$res_term = $wpdb->get_results( $sql, ARRAY_A );
				foreach ( $res_term as $key => $val ) {
					$res_arr[] = $val['term_taxonomy_id'];
				}
			}

			foreach ( $res_arr as $val ):

				$post_items = '';
				// build query
				$args = array(
					'post_type'           => $post_type_cats,
					'posts_per_page'      => $number_cats,
					'tax_query'           => array(
						array(
							'taxonomy' => $post_type_cats . '_category',
							'field'    => 'term_id',
							'terms'    => $val,
						)
					),
					'ignore_sticky_posts' => true
				);

				if ( ! empty( $exclude_cats ) ) {
					$args['category__not_in'] = explode( ',', $exclude_cats );
				}
				$posts_query = new WP_Query( $args );


				$cat_link        = '';
				$cat_title       = '';
				$post_categories = get_term_by( 'term_taxonomy_id', $val );
				/*
				 * WP_Term Object
(
	[term_id] => 15
	[name] => Careers
	[slug] => careers
	[term_group] => 0
	[term_taxonomy_id] => 15
	[taxonomy] => faq_category
	[description] =>
	[parent] => 0
	[count] => 6
	[filter] => raw
)*/
				/*echo '<pre>';
				 print_r($post_categories);
				 echo '</pre>';die;*/
				$post_index = 0;
				$post_items = '';
				while ( $posts_query->have_posts() ) {
					$posts_query->the_post();
					$post_index ++;
					$post_items .= sprintf( '<li class="post hover-light">
                                <h6><a class="title-cats" href="%1$s">%2$s</a></h6></li>',
						get_permalink(), get_the_title() );
				}
				echo sprintf( '<div class="recent-cats"><div class="recent-cats-title"><a href="%1$s"><h5>%2$s</h5></a></div><div class="recent-cats-body"><ul>%3$s</ul></div></div>', get_term_link( $post_categories ), $post_categories->name, $post_items );
			endforeach;

			print( $after_widget );

			wp_reset_postdata();

		}

		function update( $new_instance, $old_instance ) {

			$instance                   = $old_instance;
			$instance['title']          = sanitize_text_field( $new_instance['title'] );
			$instance['number_cats']    = sanitize_text_field( $new_instance['number_cats'] );
			$instance['exclude_cats']   = sanitize_text_field( $new_instance['exclude_cats'] );
			$instance['post_type_cats'] = sanitize_text_field( $new_instance['post_type_cats'] );
			$instance['order_cat']      = sanitize_text_field( $new_instance['order_cat'] );
			$instance['style']          = sanitize_text_field( $new_instance['style'] );

			return $instance;
		}

		function form( $instance ) {
			//Output admin widget options form
			extract( shortcode_atts( array(
				'title'          => '',
				'number_cats'    => 5,
				'exclude_cats'   => '',
				'post_type_cats' => '',
				'order_cat'      => '',
				'style'          => '',
			), $instance ) );
			?>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( "Title:", 'themetonaddon' ); ?></label>
                <input type="text" class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"
                       name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>"
                       value="<?php echo esc_attr( $title ); ?>"/>
            </p>
            <p>
                <input type="text" id="<?php echo esc_attr( $this->get_field_id( 'number_cats' ) ); ?>"
                       name="<?php echo esc_attr( $this->get_field_name( 'number_cats' ) ); ?>"
                       value="<?php echo esc_attr( $number_cats ); ?>" size="3"/>
                <label for="<?php echo esc_attr( $this->get_field_id( 'number_cats' ) ); ?>">Number of cats to
                    show</label>
            </p>
            <p>
				<?php
				$list_post_type = ( new TT_Post_type_PT() )->post_types;
				?>

                <select name="<?php echo esc_attr( $this->get_field_name( 'post_type_cats' ) ); ?>"
                        id="<?php echo esc_attr( $this->get_field_id( 'post_type_cats' ) ); ?>">
					<?php foreach ( $list_post_type as $key => $value ) {

						$selected = $post_type_cats == $key ? "selected" : '';

						?>
                        <option <?php echo $selected ?> value="<?php echo $key ?>"><?php echo $value ?></option>
						<?php
					}
					?>

                </select>
                <label for="<?php echo esc_attr( $this->get_field_id( 'post_type_cats' ) ); ?>">Post Type To Show
                    Cats</label>
                <br>

            </p>
            <p>

                <input type="text" id="<?php echo esc_attr( $this->get_field_id( 'order_cat' ) ); ?>"
                       name="<?php echo esc_attr( $this->get_field_name( 'order_cat' ) ); ?>"
                       value="<?php echo esc_attr( $order_cat ); ?>" size="3"/>
                <label for="<?php echo esc_attr( $this->get_field_id( 'order_cat' ) ); ?>">Order category</label>
                <br>
                <small>You can order your multiple categories with comma separation.</small>
            </p>
            <p>
                <input type="text" id="<?php echo esc_attr( $this->get_field_id( 'exclude_cats' ) ); ?>"
                       name="<?php echo esc_attr( $this->get_field_name( 'exclude_cats' ) ); ?>"
                       value="<?php echo esc_attr( $exclude_cats ); ?>" size="3"/>
                <label for="<?php echo esc_attr( $this->get_field_id( 'exclude_cats' ) ); ?>">Exclude category ID
                    (optional)</label>
                <br>
                <small>You can include multiple categories with comma separation.</small>
            </p>

			<?php
		}
	}
}

return register_widget( "Themeton_Listpost_Category_Widget" );;