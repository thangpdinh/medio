<?php
/**
 * Date: 12/3/19
 * Time: 2:07 PM
 */


if ( ! function_exists( 'medio_child_register_sidebar' ) ) {
	function medio_child_register_sidebar( $arr_sidebar ) {
		return array_merge( $arr_sidebar, array( 'service-sidebar' => 'Service sidebar' ) );
	}

	add_filter( 'medio_register_sideber', 'medio_child_register_sidebar' );

}
